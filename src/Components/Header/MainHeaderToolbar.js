import React from 'react';
import {Header, Left, Body, Right, Title, Subtitle, Button, Icon} from 'native-base';
import {TouchableOpacity} from 'react-native';
export default class MainHeaderToolbar extends React.PureComponent {

    render() {
        let back_icon = null;

        if(this.props.showBackButton){
            back_icon= (
                <Button transparent onPress={()=> this.props.navigation.goBack()}>
                    <TouchableOpacity onPress={()=> this.props.navigation.goBack()}>
                        <Icon type='Ionicons' name='ios-arrow-back' />
                    </TouchableOpacity>
                </Button>
            );
        }

        return (
            <Header>
                <Left>
                    {back_icon}
                </Left>
                <Body>
                <Title>Title</Title>
                <Subtitle>Subtitle</Subtitle>
                </Body>
                <Right/>
            </Header>

        );
    }
}




