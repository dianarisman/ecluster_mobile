import React, { Component } from 'react';
import {RefreshControl} from "react-native";
import StyleData from '../../Styles/StyleData';
import LabelData from '../../Labels/LabelData';

export default class RefreshController extends React.PureComponent {
    render() {
        return (
            <RefreshControl
                refreshing={this.props.isRefreshing}
                onRefresh={this.props.onRefreshStart}
                tintColor={StyleData.color.colorButton1}
                title={LabelData.label.loading}
                titleColor={StyleData.color.colorButton1}
                colors={[StyleData.color.colorButton1]}
                progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
            />

        );
    }
}
