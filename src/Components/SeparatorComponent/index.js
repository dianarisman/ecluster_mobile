import React, { Component } from 'react';
import {View} from 'react-native';
import StyleData from "../../Styles/StyleData";
export default class SeparatorComponent extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let size = this.props.size;
        return (
            <View style={StyleData.mainStyle.separatorMargin[size]} />
        );
    }
}
