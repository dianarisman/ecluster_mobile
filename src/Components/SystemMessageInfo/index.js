import React from 'react';
import { Card, CardItem, Body, Text } from 'native-base';
import {TouchableOpacity, View} from 'react-native';
import LabelData from "../../Labels/LabelData";
export default class SystemMessageInfo extends React.PureComponent {

    render() {
        return (
            <Card>
                <CardItem button onPress={() => alert("This is Card Header")}>
                    <Body>
                    <Text>
                        {LabelData.message.monthlyUnpaid}
                    </Text>
                    </Body>
                </CardItem>
            </Card>
        );
    }
}




