import { Platform, Dimensions, PixelRatio } from "react-native";

const mainStyle = {
    lineFlatlistSeperator: {
        height: 0.5,
        width: "100%",
        backgroundColor:"rgba(255,255,255,0.5)"
    },
    square: (val) =>({
        width: val,
        height: val
    }),
    centerItem: {
        flex: 1, justifyContent: "center", alignItems: "center"
    },
    centerItem2: {
        justifyContent: "center", alignItems: "center"
    },
    card: {
        marginVertical: 5,
        marginHorizontal: 2,
        borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
        borderLeftWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
        borderRightWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
        borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
        borderRadius: 2,
        borderColor: "#ccc",
        flexWrap: "nowrap",
        backgroundColor: "#fff",
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 1.5,
        elevation: 3
    },
    mainButton: (color) =>({
        borderRadius: 7,
        marginTop: 10,
        paddingVertical: 10,
        backgroundColor: color,
        alignItems: 'center'
    }),
    mainButtonText: (color) =>({
        margin: 2,
        color: color,
        textTransform: 'uppercase'
    }),
    showPasswordButton: {
        top: 8,
        right: 0,
        position: 'absolute',
        paddingVertical: 3,
        paddingHorizontal: 10,
        borderRadius: 5,
        backgroundColor: '#e9e9e9'
    },
    showPasswordText: {
        fontSize: 12
    },
    margin: {
        small: 5,
        medium: 10,
        large: 15
    },
    separatorMargin: {
        small: {
            marginVertical: 5
        },
        medium: {
            marginVertical: 10
        },
        large: {
            marginVertical: 15
        }
    }
};

const color = {
    color1: '#eaeaea',
    colorButton1: '#ff8e85',
    backgroundColorProgressCircle: '#f9f9f9',
};

export default styles = {
    mainStyle: mainStyle,
    color: color,
}
