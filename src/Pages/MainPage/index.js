import React, { Component } from 'react';
import Tab1 from './TabComponentPage/Tab1';
import Tab2 from './TabComponentPage/Tab2';
import Tab3 from './TabComponentPage/Tab3';
import Tab4 from './TabComponentPage/Tab4';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import TabBar from "../../Components/FooterTab";
import Tab5 from "./TabComponentPage/Tab5";


export default createAppContainer(createBottomTabNavigator({
        Tab1: { screen: Tab1 },
        Tab2: { screen: Tab2 },
        Tab3: { screen: Tab3 },
        Tab4: { screen: Tab4 },
        Tab5: { screen: Tab5 }
    },
    {
        lazy: false,
        tabBarPosition: 'bottom',
        showIcon: false,
        swipeEnabled: true,
        tabBarComponent: props => {
            return (
                <TabBar {...props}/>
            );
        }
    }
));
