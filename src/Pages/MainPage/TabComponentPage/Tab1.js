import React, { Component } from 'react';
import {Container, Content, Text, Card, Body, CardItem, Button} from 'native-base';
import {RefreshControl, View, Dimensions,TouchableNativeFeedback,Animated,Easing} from "react-native";
import DataServices from "../../../Services/DataServices";
import StyleData from "../../../Styles/StyleData";
import MainHeaderToolbar from "../../../Components/Header/MainHeaderToolbar";
import LabelData from '../../../Labels/LabelData';
import SystemMessageInfo from "../../../Components/SystemMessageInfo";
import SeparatorComponent from "../../../Components/SeparatorComponent";
import FirebaseChecker from "../../TestingPage/FirebaseChecker";
const { width: SCREEN_WIDTH }= Dimensions.get('window');
const squareScreenDim= (SCREEN_WIDTH/2)-50;
export default class Tab1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
            randomNumber: (Math.random()*100)*15000
        };
        this.mounted = false;
        this.animatedValue = new Animated.Value(0)
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({
            isLoading: true,
            randomNumber: (Math.random()*100)*15000
        });
        this.getAuth();
    };

    handleAnimation = () => {
        // A loop is needed for continuous animation
        Animated.loop(
            // Animation consists of a sequence of steps
            Animated.sequence([
                // start rotation in one direction (only half the time is needed)
                Animated.timing(this.animatedValue, {toValue: 1.0, duration: 150, easing: Easing.linear, useNativeDriver: true}),
                // rotate in other direction, to minimum value (= twice the duration of above)
                Animated.timing(this.animatedValue, {toValue: -1.0, duration: 300, easing: Easing.linear, useNativeDriver: true}),
                // return to begin position
                Animated.timing(this.animatedValue, {toValue: 0.0, duration: 150, easing: Easing.linear, useNativeDriver: true})
            ])
        ).start();
    };

    render() {
        return (
            <Container>
                <MainHeaderToolbar/>

                <Content
                    style={{backgroundColor: StyleData.color.color1}}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={StyleData.color.colorButton1}
                            title={LabelData.label.loading}
                            titleColor={StyleData.color.colorButton1}
                            colors={[StyleData.color.colorButton1, '#48649a', '#9af337','#9a8f38', '#c472f3']}
                            progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                        />}
                >
                    <Animated.View
                        style={{
                            transform: [{
                                rotate: this.animatedValue.interpolate({
                                    inputRange: [-1, 1],
                                    outputRange: ['-0.1rad', '0.1rad']
                                })
                            }]
                        }}
                    >
                        <SystemMessageInfo/>


                        <Card>
                            <CardItem header>
                                <Text>Header</Text>
                            </CardItem>
                            <CardItem>
                                <Body>
                                <Text style={{fontSize: 30}}>
                                    Rp. {DataServices.numberFormat(this.state.randomNumber)}
                                </Text>
                                </Body>
                            </CardItem>
                            <CardItem footer>
                                <Text>Footer</Text>
                            </CardItem>
                        </Card>
                        <SeparatorComponent size='large'/>
                        <View style={{
                            ...StyleData.mainStyle.centerItem
                        }}>
                            <View style={{
                                width: squareScreenDim*1.3,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between'}}>
                                <Button
                                    onPress={()=>this.props.navigation.navigate('Pay')}
                                    light
                                    style={{
                                        ...StyleData.mainStyle.square(squareScreenDim/1.7),
                                        ...StyleData.mainStyle.centerItem2
                                    }}>
                                    <Text>
                                        {LabelData.label.pay}
                                    </Text>
                                </Button>
                                <Button
                                    onPress={()=>this.props.navigation.navigate('Service')}
                                    light style={{
                                    ...StyleData.mainStyle.square(squareScreenDim/1.7),
                                    ...StyleData.mainStyle.centerItem2
                                }}>
                                    <Text>{LabelData.label.service}</Text>
                                </Button>
                            </View>
                            <SeparatorComponent size='large'/>
                            <View style={{
                                width: squareScreenDim*1.3,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between'}}>
                                <Button
                                    onPress={()=>this.props.navigation.navigate('Report')}
                                    light style={{
                                    ...StyleData.mainStyle.square(squareScreenDim/1.7),
                                    ...StyleData.mainStyle.centerItem2
                                }}>
                                    <Text>
                                        {LabelData.label.report}
                                    </Text>
                                </Button>
                                <Button
                                    onPress={()=>this.props.navigation.navigate('Donation')}
                                    light style={{
                                    ...StyleData.mainStyle.square(squareScreenDim/1.7),
                                    ...StyleData.mainStyle.centerItem2
                                }}>
                                    <Text>{LabelData.label.donation}</Text>
                                </Button>
                            </View>
                        </View>

                        <Button onPress={()=> this.handleAnimation()}
                                block light style={StyleData.mainStyle.separatorMargin.large}>
                            <Text>Emergency</Text>
                        </Button>

                        <Button
                            disabled={this.state.isLoading}
                            onPress={()=>this.props.navigation.navigate('Testing1', {
                                param1: 'ini parameter1',
                                param2: 'Hesoyam',
                                param3: new Date().getTime()
                            })}
                            block light><Text>Navigation to Other</Text></Button>

                        <Button
                            disabled={this.state.isLoading}
                            onPress={()=>this.props.navigation.navigate('ToolbarCircularBottom')}
                            block light><Text>ToolbarCircularBottom</Text>
                        </Button>

                        <Button
                            disabled={this.state.isLoading}
                            onPress={()=>this.props.navigation.navigate('ToolbarCollapsing')}
                            block light><Text>ToolbarCollapsing</Text>
                        </Button>

                        <Button
                            disabled={this.state.isLoading}
                            onPress={()=>this.props.navigation.navigate('FirebaseChecker')}
                            block light><Text>FirebaseChecker</Text>
                        </Button>

                    </Animated.View>
                </Content>

            </Container>
        );
    }
}
