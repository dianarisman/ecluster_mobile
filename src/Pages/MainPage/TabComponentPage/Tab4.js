import React, { Component } from 'react';
import { Container, Content, Text, Card, Body, CardItem, Accordion, List} from 'native-base';
import {RefreshControl,ScrollView} from "react-native";
import DataServices from "../../../Services/DataServices";
import StyleData from "../../../Styles/StyleData";
import MainHeaderToolbar from "../../../Components/Header/MainHeaderToolbar";
import LabelData from '../../../Labels/LabelData';
export default class Tab4 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
        };
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    render() {
        const dataArray = [
            { title: "First Element", content: "Lorem ipsum dolor sit amet" },
            { title: "Second Element", content: "Lorem ipsum dolor sit amet" },
            { title: "Third Element", content: "Lorem ipsum dolor sit amet" }
        ];
        return (
            <Container>
                <MainHeaderToolbar/>
                <Content
                    contentContainerStyle={{backgroundColor: StyleData.color.color1, flex: 1 }}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={StyleData.color.colorButton1}
                            title={LabelData.label.loading}
                            titleColor={StyleData.color.colorButton1}
                            colors={[StyleData.color.colorButton1]}
                            progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                        />}
                >
                    <ScrollView>
                        <Card>
                            <CardItem header>
                                <Text>Payment {(Math.random() * 1000).toFixed(2)}</Text>
                            </CardItem>
                            <CardItem>
                                <Body>
                                <Accordion
                                    style={{width: '100%'}}
                                    dataArray={dataArray}
                                    headerStyle={{ backgroundColor: "#fff" }}
                                    contentStyle={{ backgroundColor: "#ebebeb" }}
                                />
                                </Body>
                            </CardItem>
                        </Card>

                        <Card>
                            <CardItem header>
                                <Text>Payment {(Math.random() * 1000).toFixed(2)}</Text>
                            </CardItem>
                            <CardItem>
                                <Body>
                                <Accordion
                                    style={{width: '100%'}}
                                    dataArray={dataArray}
                                    headerStyle={{ backgroundColor: "#fff" }}
                                    contentStyle={{ backgroundColor: "#ebebeb" }}
                                />
                                </Body>
                            </CardItem>
                        </Card>

                        <Card>
                            <CardItem header>
                                <Text>Payment {(Math.random() * 1000).toFixed(2)}</Text>
                            </CardItem>
                            <CardItem>
                                <Body>
                                <Accordion
                                    style={{width: '100%'}}
                                    dataArray={dataArray}
                                    headerStyle={{ backgroundColor: "#fff" }}
                                    contentStyle={{ backgroundColor: "#ebebeb" }}
                                />
                                </Body>
                            </CardItem>
                        </Card>

                        <Card>
                            <CardItem header>
                                <Text>Payment {(Math.random() * 1000).toFixed(2)}</Text>
                            </CardItem>
                            <CardItem>
                                <Body>
                                <Accordion
                                    style={{width: '100%'}}
                                    dataArray={dataArray}
                                    headerStyle={{ backgroundColor: "#fff" }}
                                    contentStyle={{ backgroundColor: "#ebebeb" }}
                                />
                                </Body>
                            </CardItem>
                        </Card>

                        <Card>
                            <CardItem header>
                                <Text>Payment {(Math.random() * 1000).toFixed(2)}</Text>
                            </CardItem>
                            <CardItem>
                                <Body>
                                <Accordion
                                    style={{width: '100%'}}
                                    dataArray={dataArray}
                                    headerStyle={{ backgroundColor: "#fff" }}
                                    contentStyle={{ backgroundColor: "#ebebeb" }}
                                />
                                </Body>
                            </CardItem>
                        </Card>

                        <Card>
                            <CardItem header>
                                <Text>Payment {(Math.random() * 1000).toFixed(2)}</Text>
                            </CardItem>
                            <CardItem>
                                <Body>
                                <Accordion
                                    style={{width: '100%'}}
                                    dataArray={dataArray}
                                    headerStyle={{ backgroundColor: "#fff" }}
                                    contentStyle={{ backgroundColor: "#ebebeb" }}
                                />
                                </Body>
                            </CardItem>
                        </Card>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}
