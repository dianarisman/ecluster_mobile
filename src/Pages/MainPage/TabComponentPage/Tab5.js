import React, { Component } from 'react';
import {Thumbnail, Container, Content, Text, Card, Body, CardItem, Button, ListItem, List, Left, Right, Icon, Switch} from 'native-base';
import {RefreshControl} from "react-native";
import DataServices from "../../../Services/DataServices";
import MainHeaderToolbar from "../../../Components/Header/MainHeaderToolbar";
import LabelData from '../../../Labels/LabelData';
import StyleData from "../../../Styles/StyleData";
export default class Tab5 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
        };
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    _onLogout() {
        DataServices.logout().then(()=>{
            this.props.navigation.replace('Login')
        })
    }

    render() {
        return (
            <Container>
                <MainHeaderToolbar/>
                <Content
                    contentContainerStyle={{ flex: 1 }}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={StyleData.color.colorButton1}
                            title={LabelData.label.loading}
                            titleColor={StyleData.color.colorButton1}
                            colors={[StyleData.color.colorButton1]}
                            progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                        />}
                >
                    <ListItem thumbnail>
                        <Left>
                            <Thumbnail square source={{ uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png' }} />
                        </Left>
                        <Body>
                        <Text>Raditsan</Text>
                        <Text note numberOfLines={1}>raditsan2@gmail.com</Text>
                        </Body>
                        <Right>
                            <Button iconLeft transparent dark>
                                <Icon name='md-settings' type='Ionicons' />
                            </Button>
                        </Right>
                    </ListItem>
                    <List>
                        <ListItem itemDivider>
                            <Text uppercase style={{color: '#808080'}}>Account</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Aaron Bennet</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Ali Connors</Text>
                        </ListItem>
                        <ListItem itemDivider>
                            <Text uppercase style={{color: '#808080'}}>More</Text>
                        </ListItem>
                        <ListItem>
                            <Text>Bradley Horowitz</Text>
                        </ListItem>
                        <ListItem
                            disabled={this.state.isLoading}
                            onPress={()=>this._onLogout()}
                        >
                            {DataServices.renderButtonLoadingText(this.state.isLoading, LabelData.label.logout)}
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}
