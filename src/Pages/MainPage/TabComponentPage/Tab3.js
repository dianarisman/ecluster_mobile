import React, { Component } from 'react';
import {
    Container,
    Content,
    Text,
    Card,
    Body,
    CardItem,
    List,
    ListItem,
    Left,
    Right,
    Thumbnail,
    CheckBox, Icon
} from 'native-base';
import {RefreshControl, ScrollView, KeyboardAvoidingView} from "react-native";
import DataServices from "../../../Services/DataServices";
import StyleData from "../../../Styles/StyleData";
import MainHeaderToolbar from "../../../Components/Header/MainHeaderToolbar";
import {FlatList, View} from "react-native";
import LabelData from '../../../Labels/LabelData';
export default class Tab3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
            dummyData: []
        };
        this.mounted = false;
    }

    componentWillMount() {
        this.initDummyList();
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    isCloseToBottom({layoutMeasurement, contentOffset, contentSize}){
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 20;
    }



    isCloseToTop({layoutMeasurement, contentOffset, contentSize}){
        return contentOffset.y === 0;
    }

    initDummyList() {
        let dummyData = [];
        const min=1;
        const max=99;

        for(let i=1;i<=30;i++){
            let random = Math.random() * (+max - +min) + +min;
            dummyData.push({
                id: i,
                title: "Notif "+(Math.random() * 1000).toFixed(2),
                thumbnail: "https://randomuser.me/api/portraits/women/"+random.toFixed(0)+".jpg"

            });
        }
        this.setState({
            dummyData: dummyData
        })
    }

    renderItem = data => (
        <ListItem avatar button={true} onPress={() => console.log(data)}>
            <Left>
                <Thumbnail source={{ uri: data.item.thumbnail }} />
            </Left>
            <Body>
            <Text>{data.item.title}</Text>
            <Text note>Doing what you like will always keep you happy . .</Text>
            </Body>
            <Right>
                <Text note>3:43 pm</Text>
            </Right>
        </ListItem>);

    render() {
        return (
            <Container>
                <MainHeaderToolbar/>
                <Content
                    scrollEnabled={false}
                    contentContainerStyle={{backgroundColor: StyleData.color.color1, flex: 1, zIndex: -1 }}

                >
                    <ScrollView
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isLoading}
                                onRefresh={this._onContentRefresh}
                                tintColor={StyleData.color.colorButton1}
                                title={LabelData.label.loading}
                                titleColor={StyleData.color.colorButton1}
                                colors={[StyleData.color.colorButton1]}
                                progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                            />}
                        scrollEventThrottle={16}
                        onMomentumScrollEnd={({nativeEvent})=>{
                            /*if(this.isCloseToTop(nativeEvent)){
                                //do something
                            }*/
                            if(this.isCloseToBottom(nativeEvent)){
                                alert('a')
                            }
                        }}>
                        <FlatList
                            data={this.state.dummyData}
                            renderItem={item => this.renderItem(item)}
                            keyExtractor={item => item.id.toString()}
                            extraData={this.state}
                        />
                    </ScrollView>

                </Content>
            </Container>
        );
    }
}
