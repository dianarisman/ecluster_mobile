import React, { Component } from 'react';
import {Container, Content, Text, Card, Body, CardItem, Item, Input, Textarea, List, View, Button, Picker} from 'native-base';
import {RefreshControl} from "react-native";
import DataServices from "../../Services/DataServices";
import LabelData from '../../Labels/LabelData';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
import StyleData from "../../Styles/StyleData";
export default class DonationPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
            selected: "key1"
        };
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    _onSubmitReport() {
        this.setState({isLoading: true});
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            DataServices.toastHandler(LabelData.label.success, 'success', 'top');
            this.props.navigation.replace('Main');
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        });
    }
    onValueChange(value: string) {
        this.setState({
            selected: value
        });
    }
    render() {
        return (
            <Container>
                <MainHeaderToolbar
                    navigation={this.props.navigation}
                    showBackButton={true}
                />
                <Content
                    contentContainerStyle={{backgroundColor: StyleData.color.color1, flex: 1 }}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={StyleData.color.colorButton1}
                            title={LabelData.label.loading}
                            titleColor={StyleData.color.colorButton1}
                            colors={[StyleData.color.colorButton1]}
                            progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                        />}
                >
                    <List>
                        <Item>
                            <Picker
                                mode="dropdown"
                                selectedValue={this.state.selected}
                                onValueChange={this.onValueChange.bind(this)}
                            >
                                <Picker.Item label="Hut RI" value="key0" />
                                <Picker.Item label="Kerja Bakti" value="key1" />
                                <Picker.Item label="Kebersihan" value="key2" />
                                <Picker.Item label="Kekotoran" value="key3" />
                                <Picker.Item label="Mabar Skuy" value="key4" />
                            </Picker>
                        </Item>
                        <Item>
                            <Input placeholder='Textbox 1' />
                        </Item>
                        <Item>
                            <Input placeholder='Textbox 2' />
                        </Item>
                    </List>
                    <View style={{position: 'absolute', bottom: 10, left: 10, right: 10}}>
                        <Button light block onPress={()=> this._onSubmitReport()}>
                            <Text>{LabelData.label.send}</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}
