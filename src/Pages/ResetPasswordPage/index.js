import React, { Component } from 'react';
import {Container, Content, Text, Card, Body, CardItem, Item, Label, Input, List, Button, ListItem, Left, Right, Icon} from 'native-base';
import {RefreshControl, View} from "react-native";
import DataServices from "../../Services/DataServices";
import LabelData from '../../Labels/LabelData';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
import StyleData from "../../Styles/StyleData";
export default class ResetPasswordPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: ''
        };
    }

    _onSend() {
        if(this.state.username.trim().length === 0) {
            alert(LabelData.error_message.emptyTextInput);
            return;
        }
        alert(this.state.username);
        DataServices.toastHandler(LabelData.toast_message.success,'success','top');
    }

    render() {
        return (
            <Container>
                <MainHeaderToolbar
                    navigation={this.props.navigation}
                    showBackButton={true}
                />
                <Content padder>
                    <List>
                        <Item floatingLabel>
                            <Label>{LabelData.placeholder.username}</Label>
                            <Input
                                value={this.state.username}
                                onChangeText={(text) => this.setState({username: text})}
                            />
                        </Item>
                    </List>
                    <View style={StyleData.mainStyle.separatorMargin.large} />
                    <Button
                        disabled={this.state.isLoading}
                        onPress={()=>this._onSend()}
                        block primary><Text>{LabelData.label.send}</Text></Button>
                </Content>
            </Container>
        );
    }
}
