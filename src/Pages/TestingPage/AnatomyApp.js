import React, { Component } from 'react';
import { Container, Content, Text, Card, Body, CardItem} from 'native-base';
import {RefreshControl} from "react-native";
import DataServices from "../../Services/DataServices";
import LabelData from '../../Labels/LabelData';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
import StyleData from "../../Styles/StyleData";
export default class AnatomyApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
        };
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    render() {
        return (
            <Container>
                <MainHeaderToolbar
                    navigation={this.props.navigation}
                    showBackButton={true}
                />
                <Content
                    contentContainerStyle={{backgroundColor: StyleData.color.color1, flex: 1 }}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={StyleData.color.colorButton1}
                            title={LabelData.label.loading}
                            titleColor={StyleData.color.colorButton1}
                            colors={[StyleData.color.colorButton1]}
                            progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                        />}
                >
                    <Card>
                        <CardItem header>
                            <Text>PAGE ROUTE => {this.props.navigation.state.routeName}</Text>
                        </CardItem>
                        <CardItem>
                            <Body>
                            <Text>
                                Test Request: {JSON.stringify(this.state.dataRequest, null,3)}
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>User Token: {this.state.user_token}</Text>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}
