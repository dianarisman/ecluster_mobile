import React from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import StylesData from '../../Styles/StyleData'
export default class TestingPage1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            var1: this.props.navigation.getParam('param1', 'default data'),
            var2: this.props.navigation.getParam('param2', 'default data'),
            var3: this.props.navigation.getParam('param3', 'default data'),
            var4: this.props.navigation.getParam('param4', 'default data'),
        }
    }

    render() {
        return (
            <Container>
                <Header
                    androidStatusBarColor={StylesData.color.colorButton1}
                    style={{backgroundColor: StylesData.color.colorButton1}}
                    navigation={this.props.navigation}>
                    <Left>
                        <Button transparent onPress={()=> this.props.navigation.goBack()}
                            >
                            <Icon type='Ionicons' name='ios-arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                    <Title>Header</Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                    <Text>Param1 : {this.state.var1}</Text>
                    <Text>Param2 : {this.state.var2}</Text>
                    <Text>Param3 : {this.state.var3}</Text>
                    <Text>Param4 : {this.state.var4}</Text>
                </Content>
            </Container>
        );
    }
}
