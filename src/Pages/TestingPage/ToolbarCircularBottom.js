import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    Image,
    FlatList,
    AsyncStorage
} from 'react-native';

const window = Dimensions.get('window');

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
        'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

let localizedString;

type Props = {};
export default class ToolbarCircularBottom extends Component<Props> {

    constructor(Props){
        super(Props);
        this.state={
            collapse: false
        };
        this.collapseEnable =this.collapseEnable.bind(this);
        this.collapseDisable =this.collapseDisable.bind(this);
    }

    collapseEnable(){
        this.setState({
            collapse:true
        })
    }

    collapseDisable(){
        this.setState({
            collapse:false
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {(this.state.collapse===false)?
                    <TouchableOpacity style={{height:40, backgroundColor:'pink', alignItems:'center', justifyContent:'center'}} onPress={()=>{this.collapseEnable()}}>
                        <Text style={{fontSize:20}}>Click here</Text>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity style={styles.container} onPress={()=>{this.collapseDisable()}}>
                        <View style={styles.background} >

                        </View>
                    </TouchableOpacity>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        alignSelf: 'center',
        width: '100%',
        overflow: 'hidden', // for hide the not important parts from circle
        height: 100
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    background: { // this shape is a circle
        borderRadius: 800, // border borderRadius same as width and height
        width: '200%',
        height: 800,
        marginLeft: -200, // reposition the circle inside parent view
        position: 'absolute',
        bottom: 0, // show the bottom part of circle
        overflow: 'hidden',
        backgroundColor:'pink' // hide not important part of image
    },
});
