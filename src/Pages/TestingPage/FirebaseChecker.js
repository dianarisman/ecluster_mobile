import React from 'react';
import { StyleSheet, Platform, Image, Text, View, ScrollView } from 'react-native';

import firebase from 'react-native-firebase';
// Optional: Flow type
import type { RemoteMessage } from 'react-native-firebase';
import type { Notification } from 'react-native-firebase';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
export default class FirebaseChecker extends React.Component {
    constructor() {
        super();
        this.state = {
            chatList: [],
            fcm_token: '',
        };
    }

    checkPermissionMessaging(){
        const enabled = firebase.messaging().hasPermission();
        if (enabled) {
            this.generateMessagingFcmToken();
        } else {
            // user doesn't have permission
            this.askPermissionMessaging();
        }
    }

    async askPermissionMessaging(){
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.generateMessagingFcmToken();
        } catch (error) {
            // User has rejected permissions
        }
    }

    generateMessagingFcmToken(){
        firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    console.log('fcm_token', fcmToken);
                    this.setState({fcm_token: fcmToken})
                    //alert(fcmToken)
                } else {
                    alert('user doesnt have a device token yet')
                    // user doesn't have a device token yet
                }
            });
    }

    notificationListener2() {
        this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
            alert(JSON.stringify(message, null, 3))
        });
    }

    componentWillUnmount() {
        this.messageListener();
        this.notificationDisplayedListener();
        this.notificationListener();
    }

    componentDidMount() {
        // TODO: You: Do firebase things
        // const { user } = await firebase.auth().signInAnonymously();
        // console.warn('User -> ', user.toJSON());

        this.checkPermissionMessaging();

        this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
            console.log('onMessage', message);
        });

        this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
            console.log('onNotificationDisplayed', notification);
            alert('onNotificationDisplayed')
            // Process your notification as required
            // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        });
        this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
            // Process your notification as required
            console.log('onNotification', notification);
            //alert(JSON.stringify(notification, null, 3))
            const notification2 = new firebase.notifications.Notification({
                sound: "default",
                show_in_foreground: true,
                show_in_background: true
            })
                .setNotificationId('notificationId')
                .setTitle('My notification title')
                .setBody('My notification body')
                .setData({
                    key1: 'value1',
                    key2: 'value2',
                });
            notification2
                .android.setChannelId('channelId')
                .android.setSmallIcon('ic_launcher');
            firebase.notifications().displayNotification(notification2)
        });

        // await firebase.analytics().logEvent('foo', { bar: '123'});
        firebase.database().ref('TesterDB/')
            .on('value', snapshot => {
                let chat_list = null;
                if(snapshot.val() !== null) {
                    chat_list = Object.entries(snapshot.val()).map(item => ({...item[1], key: item[0]}));
                } else {
                    chat_list = [];
                    //alert('data null');
                }

                this.setState({
                    chatList: chat_list
                })
            })
    }



    render() {
        return (
            <View>
                <MainHeaderToolbar
                    navigation={this.props.navigation}
                    showBackButton={true}
                />
                <ScrollView>
                    <View style={styles.container}>
                        <Image source={require('./assets/ReactNativeFirebase.png')} style={[styles.logo]}/>
                        <Text style={styles.welcome}>
                            Welcome to {'\n'} React Native Firebase
                        </Text>

                        <View style={styles.modules}>
                            <Text style={styles.modulesHeader}>The following Firebase modules are pre-installed:</Text>
                            {firebase.admob.nativeModuleExists && <Text style={styles.module}>admob()</Text>}
                            {firebase.analytics.nativeModuleExists && <Text style={styles.module}>analytics()</Text>}
                            {firebase.auth.nativeModuleExists && <Text style={styles.module}>auth()</Text>}
                            {firebase.config.nativeModuleExists && <Text style={styles.module}>config()</Text>}
                            {firebase.crashlytics.nativeModuleExists && <Text style={styles.module}>crashlytics()</Text>}
                            {firebase.database.nativeModuleExists && <Text style={styles.module}>database()</Text>}
                            {firebase.firestore.nativeModuleExists && <Text style={styles.module}>firestore()</Text>}
                            {firebase.functions.nativeModuleExists && <Text style={styles.module}>functions()</Text>}
                            {firebase.iid.nativeModuleExists && <Text style={styles.module}>iid()</Text>}
                            {firebase.invites.nativeModuleExists && <Text style={styles.module}>invites()</Text>}
                            {firebase.links.nativeModuleExists && <Text style={styles.module}>links()</Text>}
                            {firebase.messaging.nativeModuleExists && <Text style={styles.module}>messaging()</Text>}
                            {firebase.notifications.nativeModuleExists && <Text style={styles.module}>notifications()</Text>}
                            {firebase.perf.nativeModuleExists && <Text style={styles.module}>perf()</Text>}
                            {firebase.storage.nativeModuleExists && <Text style={styles.module}>storage()</Text>}
                        </View>
                    </View>
                    <View style={styles.divider}>
                        <Text style={{fontWeight: 'bold'}}>FCM Token</Text>
                        <Text>{this.state.fcm_token}</Text>
                    </View>
                    <View style={styles.divider}>
                        <Text style={{fontWeight: 'bold'}}>ChatList</Text>
                        <Text>{JSON.stringify(this.state.chatList, null, 3)}</Text>
                        <View style={{marginBottom: 100}}/>
                    </View>
                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    divider: {
        borderColor: '#e88634',
        borderBottomWidth: 2,
        margin: 10
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    logo: {
        height: 120,
        marginBottom: 16,
        marginTop: 64,
        padding: 10,
        width: 135,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    modules: {
        margin: 20,
    },
    modulesHeader: {
        fontSize: 16,
        marginBottom: 8,
    },
    module: {
        fontSize: 14,
        marginTop: 4,
        textAlign: 'center',
    }
});
