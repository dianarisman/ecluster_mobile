import React, { Component } from 'react';
import { Container, Content, Text, Card, Body, CardItem} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import {RefreshControl} from "react-native";
import DataServices from "../../Services/DataServices";
import LabelData from '../../Labels/LabelData';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
import StyleData from "../../Styles/StyleData";
import CollapsingToolbar from "../../Components/CollapsingToolbar";
export default class ToolbarCollapsing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
        };
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    render() {
        return (
            <Container>
                <CollapsingToolbar
                    leftItem={<Icon name="md-menu" size={30} color="#fff" />}
                    rightItem={<Icon name="md-create" size={30}  color="#fff" />}
                    toolbarColor='#2196f3'
                    title='Demo Toolbar'
                    src={require('../../Images/toolbar_background.jpg')}>
                    <Card>
                        <CardItem header>
                            <Text>PAGE ROUTE => {this.props.navigation.state.routeName}</Text>
                        </CardItem>
                        <CardItem>
                            <Body>
                            <Text>
                                Test Request: {JSON.stringify(this.state.dataRequest, null,3)}
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>User Token: {this.state.user_token}</Text>
                        </CardItem>
                    </Card>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>
                    <Text>
                        List Item
                    </Text>

                </CollapsingToolbar>

            </Container>
        );
    }
}
