import React from 'react';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    List,
    Form, Item, Input, Label
} from 'native-base';
import {View} from 'react-native'
import DataServices from "../../Services/DataServices";
import LabelData from '../../Labels/LabelData';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
import StyleData from "../../Styles/StyleData";
export default class LoginPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: 'root@gmail.com',
            password: 'rasmuslerdorf',
            isLoading: false
        }
    }

    _onLogin(){
        this.setState({isLoading:true});
        DataServices.login(this.state.username, this.state.password).then((result)=>{
            this.setState({isLoading:false});
            if(result.access_token){
                DataServices.AuthSave(JSON.stringify(result)).then((result)=>{
                    this.props.navigation.replace('Main');
                }).catch(err=>{
                    alert(JSON.stringify(err.message, null, 3));
                })
            }
        }).catch(err =>{
            alert(JSON.stringify(err.message, null, 3));
            DataServices.toastHandler(err.message,'danger','top');
            this.setState({isLoading:false});
        })
    }

    render() {
        return (
            <Container>
                <MainHeaderToolbar/>
                <Content padder>
                    <List>
                        <Item floatingLabel>
                            <Label>{LabelData.placeholder.username}</Label>
                            <Input
                                value={this.state.username}
                                onChangeText={(text) => this.setState({username: text})}
                            />
                        </Item>
                        <Item floatingLabel>
                            <Label>{LabelData.placeholder.password}</Label>
                            <Input
                                value={this.state.password}
                                onChangeText={(text) => this.setState({password: text})}
                                secureTextEntry={true}/>
                        </Item>
                    </List>
                    <View style={StyleData.mainStyle.separatorMargin.large} />
                    <Button
                        disabled={this.state.isLoading}
                        onPress={()=>this._onLogin()}
                        block primary>{DataServices.renderButtonLoadingText(this.state.isLoading, LabelData.label.login)}</Button>
                    <Button transparent primary
                        disabled={this.state.isLoading}
                        onPress={()=>this.props.navigation.navigate('ResetPassword')}
                        block><Text>{LabelData.label.resetPassword}</Text></Button>
                </Content>
            </Container>
        );
    }
}
