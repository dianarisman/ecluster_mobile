import React, { Component } from 'react';
import {Container, Content, Text, List, ListItem, Left, Right, Icon, CheckBox, Body} from 'native-base';
import {FlatList, RefreshControl} from "react-native";
import DataServices from "../../Services/DataServices";
import LabelData from '../../Labels/LabelData';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
import StyleData from "../../Styles/StyleData";
export default class ServicePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
            dummyData: []
        };
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
        for(let i=1;i<=10;i++){
            this.state.dummyData.push({
                id: (new Date().getTime())/(Math.random()*1000),
                name: 'Layanan '+i
            });
        }
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    renderItem = data => (
        <ListItem button={true} onPress={() => console.log(data)}>
            <Left>
            <Text>{data.item.name}</Text>
            </Left>
            <Right>
                <Icon name="arrow-forward" />
            </Right>
        </ListItem>);

    render() {
        return (
            <Container>
                <MainHeaderToolbar
                    navigation={this.props.navigation}
                    showBackButton={true}
                />
                <Content
                    contentContainerStyle={{backgroundColor: StyleData.color.color1, flex: 1 }}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={StyleData.color.colorButton1}
                            title={LabelData.label.loading}
                            titleColor={StyleData.color.colorButton1}
                            colors={[StyleData.color.colorButton1]}
                            progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                        />}
                >
                    <FlatList
                        data={this.state.dummyData}
                        renderItem={item => this.renderItem(item)}
                        keyExtractor={item => item.id.toString()}
                        extraData={this.state}
                    />
                </Content>
            </Container>
        );
    }
}
