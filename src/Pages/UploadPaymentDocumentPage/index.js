import React, { Component } from 'react';
import {Container, Content, Text, Card, Body, CardItem, Button, Icon} from 'native-base';
import {RefreshControl, View, Dimensions, TouchableOpacity, Image} from "react-native";
import DataServices from "../../Services/DataServices";
import LabelData from '../../Labels/LabelData';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
import StyleData from "../../Styles/StyleData";
const { height: SCREEN_HEIGHT, width: SCREEN_WIDTH }= Dimensions.get('window');
export default class UploadPaymentDocumentPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
            paymentData: this.props.navigation.getParam('paymentData'),
            paymentMethod: this.props.navigation.getParam('paymentMethod'),
            imageHeight: 0,
            imageWidth: 0,
            imageData: null,
            imageBase64: '',
        };
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        this.setState({isLoading: true});
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    selectPhotoTapped() {
        DataServices.takePhotoHandler().then((result) => {
            // You can also display the image using data:
            try {
                const source = {uri: 'data:image/jpeg;base64,' + result.data};
                // const source = { uri: result.uri };
                const ratio = SCREEN_WIDTH / result.width;
                const imgBase64 = 'data:image/jpeg;base64,' + result.data;
                this.setState({
                    isLoading: false,
                    imageHeight: result.height * ratio,
                    imageWidth: SCREEN_WIDTH,
                    imageData: source,
                    imageBase64: imgBase64
                });

            } catch (e) {
                DataServices.toastHandler(LabelData.error_message.somethingWrong,'danger','top');
            }

        });
    }

    _onUploadDocument() {
        if(this.state.imageBase64.length <= 0){
            DataServices.toastHandler(LabelData.message.pleaseUploadPaymentProof,'danger','top');
            return;
        }
        this.setState({isLoading: true});
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            DataServices.toastHandler(LabelData.label.success, 'success', 'top');
            this.props.navigation.replace('Main');
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        });
    }

    render() {
        return (
            <Container>
                <MainHeaderToolbar
                    navigation={this.props.navigation}
                    showBackButton={true}
                />
                <Content
                    contentContainerStyle={{backgroundColor: StyleData.color.color1, flex: 1 }}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={StyleData.color.colorButton1}
                            title={LabelData.label.loading}
                            titleColor={StyleData.color.colorButton1}
                            colors={[StyleData.color.colorButton1]}
                            progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                        />}
                >
                    <Card>
                        <CardItem button onPress={() => alert("This is Card Header")}>
                            <Body>
                            <Text>
                                {LabelData.message.pleaseUploadPaymentProof}
                            </Text>
                            </Body>
                        </CardItem>
                    </Card>

                    <View style={{
                        minHeight: SCREEN_HEIGHT-140,
                        flex: 1,
                        justifyContent: "center",
                        alignItems: "center"
                    }}>
                        <TouchableOpacity activeOpacity={0.7} onPress={this.selectPhotoTapped.bind(this)}>
                            {
                                this.state.imageData ? (<Image source={this.state.imageData}
                                                               style={{height: this.state.imageHeight,
                                                                   width: this.state.imageWidth}}/>) : (<Icon type='Entypo' name='camera' style={{color: '#ccc', fontSize: (SCREEN_HEIGHT-100)/5}}/>)
                            }
                        </TouchableOpacity>
                    </View>

                    <View style={{position: 'absolute', bottom: 10, left: 10, right: 10}}>
                        <Button light block onPress={()=> this._onUploadDocument()}>
                            <Text>{LabelData.label.upload}</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}
