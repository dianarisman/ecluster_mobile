import React, { Component } from 'react';
import { Container, Content, Text, CheckBox, Body, View, List, ListItem, Left, Right, Icon, Button} from 'native-base';
import {RefreshControl, FlatList, TouchableOpacity} from "react-native";
import DataServices from "../../Services/DataServices";
import LabelData from '../../Labels/LabelData';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
import StyleData from "../../Styles/StyleData";
export default class PayPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
            paymentTotal: 0,
            dummyData: [
                {
                    id: (new Date().getTime()/(Math.random()*1000)),
                    month: 'Januari',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'Februari',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'Maret',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'April',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'Mei',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'Juni',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'Juli',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'Agustus',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'September',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'Oktober',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'November',
                    total: 100000,
                    isSelected: false
                },
                {
                    id: (new Date().getTime())/(Math.random()*1000),
                    month: 'Desember',
                    total: 100000.75456,
                    isSelected: false
                }
            ]
        };
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object
    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    sumTotal = arr => {
        return arr.reduce((a,b) => a + b.total, 0);
    };

    renderItem = data => (
        <ListItem button={true} onPress={() => this.selectItem(data)}>
            <CheckBox color='#ccc' checked={data.item.isSelected} onPress={() => this.selectItem(data)} />
            <Body>
                <Text>{data.item.month}</Text>
            </Body>
            <Right>
                <Icon name="arrow-forward" />
            </Right>
        </ListItem>);

    selectItem = data => {
        data.item.isSelected = !data.item.isSelected;
        const index = this.state.dummyData.findIndex(
            item => data.item.id === item.id
        );
        this.state.dummyData[index] = data.item;
        this.setState({
            dummyData: this.state.dummyData,
            paymentTotal: DataServices.numberFormat(this.sumTotal(this.state.dummyData.filter(item => item.isSelected)))
        });
    };

    _navCheckoutPage() {
        if(this.state.paymentTotal === 0) {
            alert(LabelData.message.pleaseSelectItem);
            return;
        }
        this.props.navigation.navigate('Checkout', {paymentData: this.state.dummyData.filter(item => item.isSelected)})

    }

    render() {
        return (
            <Container>
                <MainHeaderToolbar
                    navigation={this.props.navigation}
                    showBackButton={true}
                />
                <Content
                    contentContainerStyle={{backgroundColor: StyleData.color.color1, flex: 1 }}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={StyleData.color.colorButton1}
                            title={LabelData.label.loading}
                            titleColor={StyleData.color.colorButton1}
                            colors={[StyleData.color.colorButton1]}
                            progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                        />}
                >
                    <FlatList
                        style={{marginBottom: 70}}
                        data={this.state.dummyData}
                        renderItem={item => this.renderItem(item)}
                        keyExtractor={item => item.id.toString()}
                        extraData={this.state}
                    />
                    <View style={{position: 'absolute', bottom: 10, left: 10, right: 10}}>
                        <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                            <Text>{LabelData.label.total} : </Text>
                            <Text>{this.state.paymentTotal}</Text>
                        </View>
                        <Button light block onPress={()=> this._navCheckoutPage()}>
                            <Text>{LabelData.label.pay}</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

/*
class TestingHighlightSelectItemFlatList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            dataSource: [],
        };
    }
    componentDidMount() {this.fetchData();}
    fetchData = () => {this.setState({loading: true});
        fetch("https://jsonplaceholder.typicode.com/photos")
            .then(response => response.json())
            .then(responseJson => {
                responseJson = responseJson.map(item => {
                    item.isSelect = false;
                    item.selectedClass = styles.list;
                    return item;
                });
                this.setState({
                    loading: false,
                    dataSource: responseJson,
                });
            }).catch(error => {this.setState({loading: false});
        });
    };
    FlatListItemSeparator = () => <View style={styles.line} />;
    selectItem = data => {
        data.item.isSelect = !data.item.isSelect;
        data.item.selectedClass = data.item.isSelect ? styles.selected: styles.list;
        const index = this.state.dataSource.findIndex(
            item => data.item.id === item.id
        );
        this.state.dataSource[index] = data.item;
        this.setState({
            dataSource: this.state.dataSource,
        });
    };
    goToStore = () => {
        alert(JSON.stringify(this.state.dataSource.filter(item => item.isSelect)));
    };
    renderItem = data => (
        <TouchableOpacity
            style={[styles.list, data.item.selectedClass]}
            onPress={() => this.selectItem(data)}
        >
            <Image
                source={{ uri: data.item.thumbnailUrl }}
                style={{ width: 40, height: 40, margin: 6 }}
            />
            <Text style={styles.lightText}>  {data.item.title.charAt(0).toUpperCase() + data.item.title.slice(1)}  </Text>
        </TouchableOpacity>);

    render() {
        const itemNumber = this.state.dataSource.filter(item => item.isSelect).length;
        if (this.state.loading) {return (
            <View style={styles.loader}>
                <ActivityIndicator size="large" color="purple" />
            </View>
        );
        }
        return (
            <View style={styles.container}>
                <Text style={styles.title}>productsAvailable</Text>
                <FlatList
                    data={this.state.dataSource}
                    ItemSeparatorComponent={this.FlatListItemSeparator}
                    renderItem={item => this.renderItem(item)}
                    keyExtractor={item => item.id.toString()}
                    extraData={this.state}
                />
                <View style={styles.numberBox}>
                    <Text style={styles.number}>{itemNumber}</Text>
                </View>
                <TouchableOpacity style={styles.icon} onPress={() => this.goToStore()}>
                    <View>
                        <Icon
                            style={{fontSize: 30,  backgroundColor: "#FA7B5F"}}
                            name="shopping-cart"
                            type="Entypo"

                        />
                    </View>
                </TouchableOpacity>
            </View>
        );}}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#192338",
        paddingVertical: 50,
        position: "relative"
    },
    title: {
        fontSize: 20,
        color: "#fff",
        textAlign: "center",
        marginBottom: 10
    },
    loader: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff"
    },
    list: {
        paddingVertical: 5,
        margin: 3,
        flexDirection: "row",
        backgroundColor: "#192338",
        justifyContent: "flex-start",
        alignItems: "center",
        zIndex: -1
    },
    lightText: {
        color: "#f7f7f7",
        width: 200,
        paddingLeft: 15,
        fontSize: 12
    },
    line: {
        height: 0.5,
        width: "100%",
        backgroundColor:"rgba(255,255,255,0.5)"
    },
    icon: {
        position: "absolute",
        bottom: 20,
        width: "100%",
        left: 290,
        zIndex: 1
    },
    numberBox: {
        position: "absolute",
        bottom: 75,
        width: 30,
        height: 30,
        borderRadius: 15,
        left: 330,
        zIndex: 3,
        backgroundColor: "#e3e3e3",
        justifyContent: "center",
        alignItems: "center"
    },
    number: {fontSize: 14,color: "#000"},
    selected: {backgroundColor: "#FA7B5F"},
});

*/