import React, { Component } from 'react';
import {
    Container,
    Content,
    Text,
    Card,
    Body,
    CardItem,
    Accordion,
    Icon,
    Left,
    Right,
    ListItem,
    Radio,
    List,
    Button
} from 'native-base';
import {RefreshControl, View, ScrollView} from "react-native";
import DataServices from "../../Services/DataServices";
import LabelData from '../../Labels/LabelData';
import MainHeaderToolbar from "../../Components/Header/MainHeaderToolbar";
import StyleData from "../../Styles/StyleData";
export default class CheckoutPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCash:false,
            isTransfer:false,
            isFetchFailed: false,
            user_token: '',
            isLoading: false,
            dataRequest: '',
            paymentData: this.props.navigation.getParam('paymentData'),
        };
        this.mounted = false;
    }

    componentDidMount() {
        this.mounted = true;
        this.getAuth();
        // set a custom param on the navigation object

    }

    getAuth(){
        this.setState({isLoading: true});
        DataServices.getAuthToken().then((resolve) => {
            DataServices.consoleLog('_getAuthToken', resolve);
            this.setState( {user_token: resolve });
            this.initData(resolve);
        });
    }

    initData(token) {
        DataServices.tes().then((result)=>{
            this.setState({isLoading: false});
            this.setState({dataRequest: result});
        }).catch(err=>{
            alert(JSON.stringify(err.message, null, 3));
            this.setState({isLoading: false});
        })
    }

    _onContentRefresh = () => {
        this.setState({isLoading: true});
        this.getAuth();
    };

    _renderHeader(item, expanded) {
        return (
            <View style={{
                flexDirection: "row",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center" ,
                backgroundColor: "#ebebeb" }}>
                <Text style={{ fontWeight: "600" }}>
                    {" "}{item.month}
                </Text>
                {expanded
                    ? <Icon style={{ fontSize: 18 }} name="remove-circle" />
                    : <Icon style={{ fontSize: 18 }} name="add-circle" />}
            </View>
        );
    }
    _renderContent(item) {
        return (
            <Text
                style={{
                    backgroundColor: "#f0f0f0",
                    padding: 10,
                    fontStyle: "italic",
                }}
            >
                {DataServices.numberFormat(item.total)}
            </Text>
        );
    }

    sumTotal = arr => {
        return arr.reduce((a,b) => a + b.total, 0);
    };

    _selectPayment(method) {
        if(method === 'cash'){
            this.setState({
                isCash: true,
                isTransfer: false,
                paymentMethod: method
            });
        } else {
            this.setState({
                isCash: false,
                isTransfer: true,
                paymentMethod: method
            });
        }
    }

    _navUploadPaymentDocument(){
        if(!this.state.isCash && !this.state.isTransfer) {
            alert(LabelData.message.pleaseSelectPaymentMethod);
            return;
        }
        this.props.navigation.navigate('UploadPaymentDocument',{
            user_token: this.state.user_token,
            paymentMethod: this.state.paymentMethod,
            paymentData: this.state.paymentData
        });
    }

    render() {
        return (
            <Container>
                <MainHeaderToolbar
                    navigation={this.props.navigation}
                    showBackButton={true}
                />
                <Content
                    contentContainerStyle={{backgroundColor: StyleData.color.color1, flex: 1 }}
                    padder
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._onContentRefresh}
                            tintColor={StyleData.color.colorButton1}
                            title={LabelData.label.loading}
                            titleColor={StyleData.color.colorButton1}
                            colors={[StyleData.color.colorButton1]}
                            progressBackgroundColor={StyleData.color.backgroundColorProgressCircle}
                        />}
                >
                    <ScrollView style={{marginBottom: 70}}>
                        <Card>
                            <CardItem header>
                                <Left>
                                    <Text>{LabelData.label.payment}</Text>
                                </Left>

                            </CardItem>
                            <CardItem>
                                <Body>
                                <Accordion
                                    style={{width: '100%'}}
                                    dataArray={this.state.paymentData}
                                    animation={true}
                                    expanded={true}
                                    renderHeader={this._renderHeader}
                                    renderContent={this._renderContent}
                                />
                                </Body>
                            </CardItem>
                            <CardItem footer>
                                <Left>
                                    <Text>{LabelData.label.total}</Text>
                                </Left>
                                <Right>
                                    <Text>{DataServices.numberFormat(this.sumTotal(this.state.paymentData))}</Text>
                                </Right>
                            </CardItem>
                        </Card>

                        <List>
                            <ListItem button onPress={()=> this._selectPayment('cash')}>
                                <Left>
                                    <Text>{LabelData.label.cash}</Text>
                                </Left>
                                <Right>
                                    <Radio selected={this.state.isCash} onPress={()=> this._selectPayment('cash')} />
                                </Right>
                            </ListItem>
                            <ListItem button onPress={()=> this._selectPayment('transfer')}>
                                <Left>
                                    <Text>{LabelData.label.transfer}</Text>
                                </Left>
                                <Right>
                                    <Radio selected={this.state.isTransfer} onPress={()=> this._selectPayment('transfer')} />
                                </Right>
                            </ListItem>
                        </List>
                    </ScrollView>



                    <View style={{position: 'absolute', bottom: 10, left: 10, right: 10}}>
                        <Button light block onPress={()=> this._navUploadPaymentDocument()}>
                            <Text>{LabelData.label.pay}</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}
