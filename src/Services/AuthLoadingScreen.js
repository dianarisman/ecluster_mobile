import React from 'react';
import {
    ActivityIndicator,
    View,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import StorageKeys from '../Utils/StorageKeys';
import StyleData from '../Styles/StyleData';
export default class AuthLoadingScreen extends React.Component {
    static navigationOptions = {
        header: null ,
    };
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentWillMount(): void {
        this._bootstrapAsync().then(()=> console.log('_bootstrapAsync'));
    }

    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
        const userToken = await AsyncStorage.getItem(StorageKeys.authKey);

        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        this.props.navigation.replace(userToken ? 'Main' : 'Login');
    };

    componentDidMount() {

    }

    componentWillUnmount(){

    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center",backgroundColor:'#ffffff' }}>
                <ActivityIndicator color={StyleData.color.colorButton1} size='large' />
            </View>
        );
    }
}
