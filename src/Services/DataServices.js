import React from 'react';
import {ActivityIndicator} from "react-native";
import {Text, Toast} from "native-base";
import AsyncStorage from '@react-native-community/async-storage';
import StorageKeys from '../Utils/StorageKeys';
import StyleData from '../Styles/StyleData';
import LabelData from '../Labels/LabelData';
import ApiData from '../Api/ApiData';
import ImagePicker from "react-native-image-picker";
const SHOW_LOG = true;
class DataServices extends React.Component {
    constructor(props) {
        super(props);
    }

    static consoleLog(title, message) {
        if(SHOW_LOG){
            console.log(title+' ',message)
        }
    }

    static tes(){
        return new Promise((resolve, reject) => {
            fetch(ApiData.testerUrl)
                .then((response) => response.json())
                .then((responseJson) => {
                    resolve(responseJson);
                })
                .catch((error) =>{
                    reject(error);
                    this.consoleLog('Catch_tes',error.message);
                });
        });
    }

    static takePhotoHandler() {
        return new Promise((resolve, reject) => {
            const options = {
                title: 'Choose option',
                quality: 0.7,
                maxWidth: 500,
                //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
                storageOptions: {
                    skipBackup: true,
                    path: 'Ecluster',
                },
            };

            ImagePicker.showImagePicker(options, (response) => {
                console.log('Response = ', response);
                if (response.didCancel) {
                    console.log('User cancelled image picker');
                } else if (response.error) {
                    reject(LabelData.error_message.imagePickerErrorMessageCode1);
                    this.consoleLog(LabelData.error_message.imagePickerErrorMessageCode1, response.error);

                } /*else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }*/ else {
                    //API Reference
                    //https://github.com/react-native-community/react-native-image-picker/blob/master/docs/Reference.md
                    resolve(response);
                    //this.toastHandler(variables.photoSaved);
                }
            });
        }, err => {
            this.toastHandler(LabelData.error_message.imagePickerErrorMessageCode2,'danger','top');
            this.consoleLog(LabelData.error_message.imagePickerErrorMessageCode2,err.message);
        }).catch(err => {
            this.toastHandler(LabelData.error_message.imagePickerErrorMessageCode3,'danger','top');
            this.consoleLog(LabelData.error_message.imagePickerErrorMessageCode3,err.message);
        })
    }

    static numberFormat(number){
        /*
        const parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
        */
        let result;
        if (number === Math.floor(number)) {
            result = number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        } else {
            result = number.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
        return result;
    }

    static login(username, password){
        return new Promise((resolve, reject) => {
            fetch(ApiData.ecluster.loginUrl, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    client_secret: 'secret1',
                    grant_type: 'password',
                    client_id: 'id1',
                    username: username,
                    password: password
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                })
                .catch((error) =>{
                    reject(error);
                    this.consoleLog('Catch_login',error.message);
                });
        })
    }

    static logout = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.removeItem(StorageKeys.authKey).then(()=>{
                    resolve(true);
                });
            } catch (error) {
                reject(error)
            }
        });
    };

    static renderButtonLoadingText(isLoading, labelText) {
        return (
            isLoading ? <ActivityIndicator size='small' color={StyleData.color.colorButton1} /> : <Text>{labelText}</Text>
        )
    }

    static AuthSave = async (authData) => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.setItem(StorageKeys.authKey, authData);
                await AsyncStorage.getItem(StorageKeys.authKey, (error, result) => {
                    if (result) {
                        resolve(result);
                    }else{
                        resolve(null)
                    }
                });
            } catch (error) {
                reject(error);
                this.consoleLog('Catch_AuthSave',error.message);
            }
        });

    };

    static getAuthToken = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                await AsyncStorage.getItem(StorageKeys.authKey, (error, result) => {
                    if (result) {
                        const authData = JSON.parse(result);
                        resolve(authData.access_token);
                    }else{
                        resolve(null)
                    }
                });
            } catch (error) {
                reject(error);
                this.consoleLog('Catch_getAuthToken',error.message);
            }
        });

    };

    static toastHandler(toastMessage, toastType, toastPosition) {
        Toast.show({
            text: toastMessage,
            duration: 3000,
            type: toastType,
            position: toastPosition ? toastPosition : 'bottom',
            buttonText: LabelData.label.ok,
            buttonTextStyle: { color: "#fff" },
            buttonStyle: { backgroundColor: 'rgba(0, 0, 0, 0.251)' }
        });
    }

    static sendNotif(title, message, to_fcm_token, user_id){
        const notification = {
            'registration_ids':[to_fcm_token],
            'data':{
                'id': user_id,
                "priority": "high",
                "collapseKey": "DM",
                "content_available": true,
                "tag": "directMessage",
                'message':message,
                'title':title,
                "show_in_foreground": true,
                'vibrate':true, //additionalData
                'sound':"default", //additionalData
            }
        };

        const header = {
            'Content-Type': 'application/json',
            'Authorization': 'key='+ApiData.firebase.serverKey
        };

        return new Promise((resolve, reject) => {
            fetch('https://fcm.googleapis.com/fcm/send', {
                method: 'POST',
                //withCredentials: true,
                headers: header,
                body: JSON.stringify(notification),
            }).then((response) => response.json())
                .then((responseJson) => {
                    // resolve(responseJson.movies);
                    resolve(responseJson);
                    //alert(JSON.stringify(responseJson))
                })
                .catch((error) =>{
                    setTimeout( () => {
                        reject(error);
                        this.sendNotif(title, message, to_fcm_token, user_id);
                    },3000);
                    console.error(error);
                });
        })
    }
}
export default DataServices;
