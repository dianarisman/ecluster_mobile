
const label = {
    notif: 'Notif',
    news: 'Berita',
    upload: 'Upload',
    payment: 'Pembaran',
    transfer: 'Transfer',
    cash: 'Cash',
    total: 'Total',
    donation: 'Sumbangan',
    service: 'Layanan',
    pay: 'Bayar',
    report: 'Lapor',
    send: 'Kirim',
    resetPassword: 'Reset Password',
    home: 'Beranda',
    ok: 'Ok',
    history: 'Riwayat',
    help: 'Bantuan',
    profile: 'Profil',
    loading: 'Loading',
    login: 'Login',
    logout: 'Logout',
    register: 'Register',
    gender: 'Jenis Kelamin',
    male: 'Pria',
    female: 'Wanita',
    username: 'Username',
    password: 'Password',
    success: 'Sukses',
    failed: 'Failed',
};

const placeholder = {
    username: label.username,
    password: label.password,
};

const message = {
    somethingWrong: 'Terjadi kesalahan, mohon coba beberapa saat kembali, atau hubungi admin',
    monthlyUnpaid: 'Silahkan bayar uang bulanan anda sebelumnya.',
    pleaseSelectItem: 'Mohon pilih terlebih dahulu.',
    pleaseSelectPaymentMethod: 'Mohon pilih metode pembayaran.',
    pleaseUploadPaymentProof: 'Mohon upload bukti pembayaran untuk melanjutkan.',
};

const toast_message = {
    success: label.success,
    failed: label.failed,
    somethingWrong: 'Ada kesalahan coba lagi'
};

const error_message = {
    somethingWrong: message.somethingWrong,
    emptyTextInput: 'Data tidak boleh kosong',
    //DON'T EDIT
    imagePickerErrorMessageCode1: 'ImagePicker Error : Code 1',
    imagePickerErrorMessageCode2: 'ImagePicker Error : Code 2',
    imagePickerErrorMessageCode3: 'ImagePicker Error : Code 3',
    //END ERROR MESSAGE
};

export default ({
    label: label,
    placeholder: placeholder,
    message: message,
    error_message: error_message,
    toast_message: toast_message
});
