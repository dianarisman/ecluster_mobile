/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import {Toast, Root, ActionSheet} from "native-base";
import LoginPage from "./src/Pages/LoginPage";
import AuthLoadingScreen from "./src/Services/AuthLoadingScreen";
import TestingPage1 from "./src/Pages/TestingPage/TestingPage1";
import MainPage from "./src/Pages/MainPage";
import AnatomyApp from "./src/Pages/TestingPage/AnatomyApp";
import ResetPasswordPage from "./src/Pages/ResetPasswordPage";
import PayPage from "./src/Pages/PayPage";
import CheckoutPage from "./src/Pages/CheckoutPage";
import UploadPaymentDocumentPage from "./src/Pages/UploadPaymentDocumentPage";
import ServicePage from "./src/Pages/ServicePage";
import DonationPage from "./src/Pages/DonationPage";
import ReportPage from "./src/Pages/ReportPage";
import ToolbarCircularBottom from "./src/Pages/TestingPage/ToolbarCircularBottom";
import ToolbarCollapsing from "./src/Pages/TestingPage/ToolbarCollapsing";
import FirebaseChecker from "./src/Pages/TestingPage/FirebaseChecker";
const RootStack = createStackNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      Login: LoginPage,
      ResetPassword: ResetPasswordPage,
      Main: MainPage,
      Testing1: TestingPage1,
      AnatomyApp: AnatomyApp,
      Pay: PayPage,
      Checkout: CheckoutPage,
      UploadPaymentDocument: UploadPaymentDocumentPage,
      Service: ServicePage,
      Donation: DonationPage,
      Report: ReportPage,
      //TESTING PAGE
        ToolbarCircularBottom: ToolbarCircularBottom,
        ToolbarCollapsing: ToolbarCollapsing,
        FirebaseChecker: FirebaseChecker

    },
    {
      initialRouteName: 'AuthLoading',
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
      }
    }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
  }

  componentWillUnmount() {
    Toast.toastInstance = null;
    ActionSheet.actionsheetInstance = null;
  }
  render() {
    return <Root><AppContainer /></Root>;
  }
}


